-- LUALOCALS < ---------------------------------------------------------
local ipairs, math, minetest, tonumber
    = ipairs, math, minetest, tonumber
local math_abs, math_floor, math_pi
    = math.abs, math.floor, math.pi
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()
local pi = math_pi

local function setting(name, def)
	return tonumber(minetest.settings:get(modname .. "_" .. name)) or def
end

local snapped = {}

local pending = {}
local function playerstep(player)
	local pname = player:get_player_name()
	local found = pending[pname]

	local bits = player:get_player_control_bits()
	-- When player first presses Aux1, capture starting angles.
	if bits == 32 then
		if not found then
			pending[pname] = {
				v = player:get_look_vertical(),
				h = player:get_look_horizontal()
			}
		end
		return
	end

	local gain = setting("volume", 1) * 0.2

	-- If player presses any other control, cancel angle snap.
	pending[pname] = nil
	if bits ~= 0 or not found then
		-- Outside of angle snapping, detect and make noise
		-- for any angle drift, up to a limit when the player
		-- is considered no longer "snapped".
		local s = snapped[pname]
		if not s then return end
		local drift = setting("driftzone", 0.02)
		if math_abs(player:get_look_vertical() - s.v) < drift then
			-- Account for 0 == 2pi
			local hdiff = math_abs(player:get_look_horizontal() - s.h)
			if hdiff > math_pi then hdiff = math_abs(hdiff - math_pi * 2) end
			if hdiff < drift then return end
		end
		if gain > 0 then
			minetest.sound_play(modname .. "_break", {
					gain = gain,
					to_player = pname
				}, true)
		end
		snapped[pname] = nil
		return
	end
	-- If player releases Aux1, apply snap.

	local sv = "mid"
	local sh = "mid"

	-- Interval of angular units to lock to
	local thetah = pi * 2 / setting("horz", 8)
	local thetav = pi / setting("vert", 4)

	-- Dead zone for detecting deliberate angle change gestures
	local dzsetting = setting("deadzone", 0.25)
	local deadzoneh = thetah * dzsetting
	local deadzonev = thetav * dzsetting

	-- If player changed pitch deliberately while holding Aux1,
	-- move angle exactly one stop
	local p = player:get_look_vertical() - found.v
	if p > deadzonev then
		found.v = found.v + thetav
		sv = "down"
	elseif p < -deadzonev then
		found.v = found.v - thetav
		sv = "up"
	end

	-- If player changed yaw deliberately while holding Aux1,
	-- move yaw exactly one stop. Account for wrap-around.
	local y = player:get_look_horizontal() - found.h
	if y > pi then y = y - pi * 2
	elseif y < -pi then y = y + pi * 2
	end
	if y > deadzoneh then
		found.h = found.h + thetah
		sh = "right"
	elseif y < -deadzoneh then
		found.h = found.h - thetah
		sh = "left"
	end

	if gain >= 0 then
		-- Always play a "camera snap" sound.
		minetest.sound_play(modname .. "_snap", {
				gain = gain,
				to_player = pname
			}, true)

		-- Play a "whoosh" sound if the camera is moving.
		if sv ~= "mid" or sh ~= "mid" then
			minetest.sound_play(modname .. "_" .. sv .. "_" .. sh, {
					gain = gain,
					to_player = pname
				}, true)
		end
	end

	-- Apply snaped in angle.
	local newv = math_floor((found.v / thetav) + 0.5) * thetav
	player:set_look_vertical(newv)
	local newh = math_floor((found.h / thetah) + 0.5) * thetah
	player:set_look_horizontal(newh)
	snapped[pname] = {h = newh, v = newv}
end

minetest.register_globalstep(function()
		for _, player in ipairs(minetest.get_connected_players()) do
			playerstep(player)
		end
	end)
