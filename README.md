This mod defines a number of discrete stops for camera angles, and allows a player to use special input sequences to:

- Snap the camera angle (pitch and yaw) to the nearest stop.
- Turn the camera (pitch, yaw, or both) one single step.

This can be particularly useful for consistent orienting of the player in sightless gaming conditions, such as:

- Games with no visuals, such as [Veil of the Unknown](https://content.minetest.net/packages/StarNinjas/veil_of_the_unknown/)
- Blindfolded speedrunning

Each camera movement will make a distinct sound.

Use with a gamepad or joystick (and AntiMicroX, if Minetest still lacks game pad support) is recommended to precisely control angle stepping inputs and avoid moving the camera accidentally.

### Control Inputs

- Stand still (pressing no other inputs) and press and hold Aux1
- To snap the camera to the nearest stop, do nothing else and release Aux1 after a short amount of time (just enough for 1 server step to recognize the input).
- To change the camera angle by 1 step, nudge the camera in the direction you want to turn before releasing Aux1.
	- 8 directions (including diagonals) are supported.
	- There is a small dead zone (nudge must be large enough)
	- Nudges should not be too large (avoid wrapping around on horizontal axis)

Note that it is currently (as of MT 5.8) not possible to change the camera angle without sending the player a position update, which will cause movement jank, and hence why the camera control gestures are not supported while moving.

### Configuration

Configuration can be accessed in the **Content: Mods** setting section of the Minetest settings menu.

The number of horizontal and vertical angle stops can be adjusted by configuration, but default to 45 degree angles.

The size of the dead zone (the amount of movement tolerated before a deliberate camera movement is detected) can be adjusted.
